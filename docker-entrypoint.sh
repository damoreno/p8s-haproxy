#!/bin/sh
set -e

# Start rsyslogd
rm -f /var/run/rsyslogd.pid
rsyslogd

# first arg is `-f` or `--some-option`
if [ "${1#-}" != "$1" ]; then
  set -- haproxy "$@"
fi

if [ "$1" = 'haproxy' ]; then
  shift
  set -- haproxy -W -db "$@"
fi

exec "$@"
