FROM gitlab-registry.cern.ch/linuxsupport/cc7-base

LABEL org.label-schema.maintainer="David Moreno García" \
      org.label-schema.name="HAProxy"

RUN yum -y upgrade && \
    yum install -y yum-plugin-priorities && \
    yum-config-manager --add-repo "http://linuxsoft.cern.ch/internal/repos/ai7-stable/x86_64/os/" && \
    yum-config-manager --setopt=linuxsoft.cern.ch_internal_repos_ai7-stable_x86_64_os_.priority=1 --save && \
    yum-config-manager --setopt=linuxsoft.cern.ch_internal_repos_ai7-stable_x86_64_os_.gpgcheck=0 --save && \
    yum clean all

RUN yum install -y ca-certificates docker rh-haproxy18 rsyslog shadow socat && \
    mkdir -p /etc/rsyslog.d && \
    mkdir -p /var/lib/rsyslog && \
    touch /var/log/haproxy.log && \
    ln -sf /dev/stdout /var/log/haproxy.log

ENV PATH="/opt/rh/rh-haproxy18/root/usr/sbin:${PATH}"

COPY docker-entrypoint.sh /

COPY files/rsyslog.conf /etc/rsyslog.d/21-haproxy.conf

ENTRYPOINT [ "/docker-entrypoint.sh" ]

CMD [ "haproxy", "-f", "/etc/haproxy/haproxy.cfg" ]
